import unittest
from app import remove_puct
import sys

class TestStringMethods(unittest.TestCase):

	def test(self):
		sem_punc = remove_puct(sys.stdin)
		punc = ['.',':','...','(',')','!','?',"'",":",'-',"'",'"']
		for letra in sem_punc:
			self.assertFalse(letra in punc)

def runTests():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestStringMethods)
    unittest.TextTestRunner(verbosity=2, failfast=True).run(suite)

if __name__ == '__main__':
    unittest.main()
